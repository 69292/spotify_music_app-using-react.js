import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from 'react';

function App() {

  const [keyword, setKeyword] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const [tracks, setTrack] = useState([]);
  
  const getTrack = async() => {
    

  let data = await fetch(
    `https://v1.nocodeapi.com/raut/spotify/xvcqiggZaUkNKPWb/search?q=${keyword === "" ? "trending" :keyword}
    &type=track`
     );

  let convertedData = await data.json();

   console.log(convertedData.tracks.items);
   setTrack(convertedData.tracks.items);
   setIsLoading(false);
  };

  
  
  return( 
  <>
    {/* we are using this NAVBAR from bootstrap doc */}
    <nav className="navbar navbar-dark navbar-expand-lg bg-dark">
  <div className="container-fluid">
    <a className="navbar-brand" href="#">
      v-music
    </a>
   
    <div className="collapse navbar-collapse deflex justify-content" 
    id="navbarSupportedContent">
      
        <input
        value={keyword}
        onChange={(event) => setKeyword(event.target.value)}
          className="form-control me-2 w-75"
          type="search"
          placeholder="Search"
          aria-label="Search"
        />
        <button onClick={getTrack} className="btn btn-outline-success"  >
          Search
        </button>
    
    </div>
  </div>
</nav>
{/* <button onClick={getTrack} className="btn btn-primary">
get Data
</button> */}
<div className="container">
  <div className=
  {`row ${isLoading ? "" : "d-none"}`}>
    <div className="col-12 py-5 text-center">
    <div
  className="spinner-border"
  style={{ width: "3rem", height: "3rem" }}
  role="status"
>
  <span className="visually-hidden">Loading...</span>
</div>

    </div>
  </div>

  <div className=
  {`row ${keyword === "" ? "" : "d-none"}`}>
    <div className="col-12 py-5 text-center">
      <h1>v-music</h1>
   
</div>

    </div>
  </div>
<div className="row">
 {tracks.map((element) => {
  return(
    <div key={element.id} className='col-lg-3 col-md-6 py-2'>
   <div className="card" >

  <img src={element.album.images[0].url}
   className="card-img-top" 
   alt="..." />

  <div className="card-body">
    <h5 className="card-title">{element.name}</h5>
    <p className="card-text">
      Artist: {element.album.artists[0].name}
    </p>

    <p className="card-text">
      Release date: {element.album.release_date}
    </p>

   <audio src={element.preview_url} 
   controls
    className="w-100"
    ></audio>
{/* we  can set thses artist,release data and audio from the api's using the browser with the help of JS*/}
  </div>
</div>
    </div>
  );
 })}

</div>

{/* </div> */}
  </>);
}

export default App;



//HTML TO JSX CONVERTER
//https://transform.tools/html-to-jsx
//for the API'S we are used noApi.com
//https://app.nocodeapi.com/dashboard/api/spotify
//in this in Q: we put singer name which song we want for testing
//and in param in type we are use track then we click om clich and test api
//and then finally we get api